FROM alpine:latest

WORKDIR /revert
COPY ./src /revert/src
COPY ./Cargo.toml /revert/Cargo.toml

ENV REVERT_BINDING=localhost:3000
ENV REVERT_SSL=f
ENV REVERT_REDIS_CONNECTION_STRING=$rediscnn
ENV REVERT_POSTGRES_CONNECTION_STRING=$pgcnn
ENV REVERT_POSTGRES_POOL_SIZE=50

RUN apk add --no-cache llvm-libunwind \
    && apk add --no-cache openssl-dev \
    && apk add --no-cache pkgconfig \
    && apk add --no-cache --virtual .build-rust rust cargo \
    && cargo build --release \
    && cp target/release/revert . \
    && rm -rf target/ ~/.cargo/ /revert/src \
    && rm /revert/Cargo.toml

RUN apk add --no-cache redis

ENTRYPOINT [ "./revert" ]