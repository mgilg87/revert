use r2d2_postgres::PostgresConnectionManager;
use r2d2::Pool;
use iron::typemap::Key;

pub struct PgConnection {
    pub pool: Pool<PostgresConnectionManager>
}

impl Key for PgConnection { type Value = PgConnection; }