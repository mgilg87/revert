use iron::typemap::Key;
use redis::{Client, cmd, RedisResult};

pub struct RedisStore {
    tiles: Client
}

impl Key for RedisStore { type Value = RedisStore; }

impl RedisStore {
    pub fn new(connection_string: &str) -> Self {
        RedisStore {
            tiles: Client::open(connection_string).unwrap()
        }
    }

    pub fn get_tile(&self, key: &str) -> Result<Vec<u8>, &'static str> {
        let cnn = self.tiles.get_connection().unwrap();
        let result: RedisResult<Vec<u8>> = cmd("GET").arg(key).query(&cnn);
        match result {
            Ok(tmp) => {
                if tmp.len() < 1 { 
                    Err("not found") 
                } else { 
                    Ok(tmp)
                }
            },
            _ => Err("not found")
        }
    }

    pub fn save_tile(&self, key: &str, tile: Vec<u8>) {
        let cnn = self.tiles.get_connection().unwrap();
        cmd("SET").arg(key).arg(tile).execute(&cnn);
    }

    pub fn invalidate(&self, key: &str) {
        let cnn = self.tiles.get_connection().unwrap();
        cmd("DEL").arg(key).execute(&cnn);
    }

    pub fn clear(&self) {
        let cnn = self.tiles.get_connection().unwrap();
        cmd("FLUSHALL").execute(&cnn);
    }
}