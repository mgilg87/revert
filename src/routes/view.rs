use router::Router;
use iron::prelude::*;
use iron::status;
use persistent::Read;
use share::{RedisStore, PgConnection};
use vectortile::{Encode, Tile, Layer, Feature};
use vectortile::grid::Grid;
use r2d2::PooledConnection;
use r2d2_postgres::PostgresConnectionManager;
use postgres::types::ToSql;

const INVALIDATE_ALL_KEY: &str = "all";

struct LayerInfo {
    name: String,
    query: String
}

pub struct VectorTiles;

fn get_layer_query(view_id: i32, cnn: &PooledConnection<PostgresConnectionManager>) -> Vec<LayerInfo> {
    let mut result = vec![];
    let view_query = "SELECT 
                        \"schema\" as s, 
                        \"table\" as t, 
                        geo_column, 
                        \"attributes\" as columns, 
                        l.title as \"layername\" 
                    FROM revert_server.\"view\" v
                    LEFT JOIN revert_server.bridge_view_layer bl ON bl.view_id = v.id
                    LEFT JOIN revert_server.layer l ON l.id = bl.layer_id
                    WHERE v.id = $1";
    let layers = cnn.query(view_query, &[&view_id]).unwrap();
    for layer in layers.iter() {
        let tmp_name = layer.get::<usize, String>(4);
        let tmp_query = format!("SELECT ST_Transform({:?}::geometry, 3857) AS geom,{} FROM {:?}.{:?} where geom && ST_Transform(ST_MakeEnvelope($1, $2, $3, $4, $5), 4326)", 
                layer.get::<usize, String>(2), layer.get::<usize, String>(3), layer.get::<usize, String>(0), layer.get::<usize, String>(1));
        result.push(LayerInfo {
            name: tmp_name,
            query: tmp_query
        });
    }
    result
}

fn create_layer<'a>(name: &'a str, cnn: &PooledConnection<PostgresConnectionManager>, query: &'a str, params: &[&ToSql]) -> Layer<'a> {
    let mut layer = Layer::new(name);
    let result = cnn.query(query, params).unwrap();
    for row in result.iter() {
        let mut feature = Feature::new(row.get(0));
        layer.add_feature(feature);
    }
    layer
}

impl VectorTiles {

    pub fn invalidate(req: &mut Request) -> IronResult<Response> {
        let redis_key = req.extensions.get::<Router>().unwrap().find("key").unwrap_or("/");
        let redis_cache = req.extensions.get::<Read<RedisStore>>().unwrap();
        if redis_key == INVALIDATE_ALL_KEY {
            redis_cache.clear();
        } else {
            redis_cache.invalidate(&redis_key);
        }
        Ok(Response::with((status::Ok, "")))
    }

    pub fn generate_view(req: &mut Request) -> IronResult<Response> {
        let view_id = req.extensions.get::<Router>().unwrap().find("viewid").unwrap_or("/");
        let z = req.extensions.get::<Router>().unwrap().find("z").unwrap_or("/");
        let x = req.extensions.get::<Router>().unwrap().find("x").unwrap_or("/");
        let y = req.extensions.get::<Router>().unwrap().find("y").unwrap_or("/");
        
        let db = req.extensions.get::<Read<PgConnection>>().unwrap();
        let cnn = db.pool.get().unwrap();
        let redis_cache = req.extensions.get::<Read<RedisStore>>().unwrap();

        let mut redis_key = String::new();
        for i in 0..3 {
            if i > 0 {
                redis_key.push_str("_");    
            }
            match i {
                0 => redis_key.push_str(view_id),
                1 => redis_key.push_str(z),
                2 => redis_key.push_str(x),
                3 => redis_key.push_str(y),
                _ => redis_key.push_str("")
            }
        }

        let parsed_view_id = view_id.parse::<i32>().unwrap();
        let parsed_z = z.parse::<u8>().unwrap();
        let parsed_x = x.parse::<u32>().unwrap();
        let parsed_y = y.parse::<u32>().unwrap();

        match redis_cache.get_tile(&redis_key) {
            Ok(tile) => Ok(Response::with((status::Ok, tile))),
            _ => {
                let grid = Grid::web_mercator();
                let bbox = grid.tile_extent(parsed_z, parsed_x, parsed_y);
                let mut layer_info = get_layer_query(parsed_view_id, &cnn);
                let mut tile = Tile::new(&bbox);

                for info in &layer_info {
                    let layer = create_layer(&info.name, &cnn, &info.query, &[&bbox.minx, &bbox.miny, &bbox.maxx, &bbox.maxy, &grid.srid]);
                    tile.add_layer(layer);
                }

                let encoded = tile.encode(&grid);
                let mut result = Vec::new();
                encoded.to_writer(&mut result).unwrap();

                redis_cache.save_tile(&redis_key, result.clone());

                Ok(Response::with((status::Ok, result)))
            }
        }
    }
}