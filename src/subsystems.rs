use iron::Chain;
use share::{RedisStore, PgConnection};
use persistent::Read;
use iron_cors::CorsMiddleware;
use r2d2::{Config, Pool};
use r2d2_postgres::{PostgresConnectionManager, TlsMode};

pub struct Linker;

impl Linker {

    pub fn init_redis_store(mut chain: Chain, connection_string: &str) -> Chain {
        let store = RedisStore::new(connection_string);
        chain.link(Read::<RedisStore>::both(store));
        chain
    }

    pub fn init_cors(mut chain: Chain) -> Chain {
        chain.link_around(CorsMiddleware::with_allow_any());
        chain
    }

    pub fn init_postgres_connection(mut chain: Chain, connection_string: &str, pool_size: u32) -> Chain {
        let config = Config::builder()
            .pool_size(pool_size)
            .build();
        let manager = PostgresConnectionManager::new(
                    connection_string,
                    TlsMode::None
                ).unwrap();
        let cnn = PgConnection {
            pool: Pool::new(config, manager).unwrap()
        };
        chain.link(Read::<PgConnection>::both(cnn));
        chain
    }
}