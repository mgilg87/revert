extern crate iron;
extern crate router;
extern crate redis;
extern crate persistent;
extern crate hyper_native_tls;
extern crate iron_cors;
extern crate r2d2;
extern crate r2d2_postgres;
extern crate postgres;
extern crate vectortile;

mod share;
mod routes;
mod subsystems;

use std::env;
use iron::prelude::*;
use iron::Chain;
use router::Router;
use subsystems::Linker;
use routes::VectorTiles;
use hyper_native_tls::NativeTlsServer;

const REVERT_BINDING: &str = "REVERT_BINDING";
const REVERT_SSL: &str = "REVERT_SSL";
const REVERT_CERT_PASS: &str = "REVERT_CERT_PASS";
const REVERT_REDIS_CONNECTION_STRING: &str = "REVERT_REDIS_CONNECTION_STRING";
const REVERT_POSTGRES_POOL_SIZE: &str = "REVERT_POSTGRES_POOL_SIZE";
const REVERT_POSTGRES_CONNECTION_STRING: &str = "REVERT_POSTGRES_CONNECTION_STRING";
const MSG_NO_ENVIROMENT: &str = " is not defined in the environment.";

fn main() {
    let mut binding = String::new();
    match env::var_os(REVERT_BINDING) {
        Some(val) => binding = val.into_string().unwrap(),
        None => println!("{} {}", REVERT_BINDING, MSG_NO_ENVIROMENT)
    }
    let mut use_ssl = String::new();
    match env::var_os(REVERT_SSL) {
        Some(val) => use_ssl = val.into_string().unwrap(),
        None => println!("{} {}", REVERT_SSL, MSG_NO_ENVIROMENT)
    }
    let mut cert_pass = String::new();
    match env::var_os(REVERT_CERT_PASS) {
        Some(val) => cert_pass = val.into_string().unwrap(),
        None => if use_ssl == "t" {
            println!("{} {}", REVERT_CERT_PASS, MSG_NO_ENVIROMENT);
        } else {
            cert_pass = "".to_owned();
        }
    }
    let mut redis_connection_string = String::new();
    match env::var_os(REVERT_REDIS_CONNECTION_STRING) {
        Some(val) => redis_connection_string = val.into_string().unwrap(),
        None => println!("{}, {}", REVERT_REDIS_CONNECTION_STRING, MSG_NO_ENVIROMENT)
    }
    let mut postgres_pool_size: u32 = 15;
    match env::var_os(REVERT_POSTGRES_POOL_SIZE) {
        Some(val) => postgres_pool_size = val.into_string().unwrap().parse::<u32>().unwrap(),
        None => println!("{} {}", REVERT_POSTGRES_POOL_SIZE, MSG_NO_ENVIROMENT)
    }
    let mut postgres_connection_string = String::new();
    match env::var_os(REVERT_POSTGRES_CONNECTION_STRING) {
        Some(val) => postgres_connection_string = val.into_string().unwrap(),
        None => println!("{} {}", REVERT_POSTGRES_CONNECTION_STRING, MSG_NO_ENVIROMENT)
    }

    let mut router = Router::new();

    router.get("/view/:viewid/:z/:x/:y", VectorTiles::generate_view, "view");
    router.get("/invalidate/:key", VectorTiles::invalidate, "invalidate");

    let chain = Chain::new(router);
    let chain = Linker::init_redis_store(chain, &redis_connection_string);
    let chain = Linker::init_postgres_connection(chain, &postgres_connection_string, postgres_pool_size);
    let chain = Linker::init_cors(chain);

    if use_ssl == "t" {
        let ssl = NativeTlsServer::new("identity.p12", &cert_pass).unwrap();
        Iron::new(chain).https(&binding, ssl).unwrap();
        println!("Server running at https://{:?}", &binding);
    } else {
        Iron::new(chain).http(&binding).unwrap();
        println!("Server running at http://{:?}", &binding);
    }
}