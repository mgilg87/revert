# Generate Certificate for testing

```bash
openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 3650 -out certificate.pem
openssl pkcs12 -inkey key.pem -in certificate.pem -export -out identity.p12
rm key.pem
rm certificate.pem
```

Put the identity.p12 Certificate in the Root folder of the Server.