# Build a Docker Image and Run

```bash
docker build -t revert \
--build-arg pgcnn=postgresql://postgres:postgres@localhost:5432/testdb?application_name=revert \
--build-arg rediscnn=http/127.0.0.1/ .
```

### Run the Image

```bash
docker run -d --name revert revert -p 3000:3000
```